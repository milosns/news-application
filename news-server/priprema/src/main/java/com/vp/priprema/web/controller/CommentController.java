package com.vp.priprema.web.controller;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vp.priprema.model.user.Comment;
import com.vp.priprema.model.user.News;
import com.vp.priprema.service.CommentService;
import com.vp.priprema.service.NewsService;
import com.vp.priprema.web.dto.CommentDTO;

@RestController
public class CommentController {
	
	@Autowired
	CommentService commentService;
	
	@Autowired
	NewsService newsService;

	
	@PostMapping("api/news/{id}")
	public ResponseEntity<CommentDTO> postComment(@PathVariable Long id, 
			@RequestBody CommentDTO commentDTO) {
		
		News news = newsService.findById(id);
		Comment comment = new Comment();
		comment.setNews(news);
		comment.setText(commentDTO.getText());
		comment.setUsername(commentDTO.getUsername());
		
		news.getComments().add(comment);
		newsService.save(news);
		
		Comment savedComment = commentService.save(comment);
		
		return new ResponseEntity<CommentDTO>(new CommentDTO(savedComment), HttpStatus.CREATED);
	}
	
	@PutMapping("api/news/{id}/comments/{id}")
	public ResponseEntity<CommentDTO> postCommentForComment(@PathVariable Long id,
			@RequestBody CommentDTO commentDTO) {
		
		Comment commentForComment = commentService.findById(id);
		
		Comment commentToAdd = new Comment();
		commentToAdd.setNews(commentForComment.getNews());
		commentToAdd.setText(commentDTO.getText());
		commentToAdd.setUsername(commentDTO.getUsername());
		
		Comment savedComment = commentService.save(commentToAdd);
		
		commentForComment.getComments().add(commentToAdd);
		commentService.save(commentForComment);
		
		return new ResponseEntity<CommentDTO>(new CommentDTO(savedComment), HttpStatus.OK);
	}
	
	@GetMapping("api/news/{id}/comments")
	public ResponseEntity<List<CommentDTO>> getAll(@PathVariable Long id) {
		List<CommentDTO> commentDTOs = newsService.findById(id).getComments()
				.stream().map(CommentDTO::new)
				.collect(Collectors.toList());
		return new ResponseEntity<List<CommentDTO>>(commentDTOs, HttpStatus.OK);
	}
	
	
	@GetMapping("api/comments")
	public ResponseEntity<List<CommentDTO>> findAll() {
		List<CommentDTO> commentDTOs = commentService.findAll()
				.stream().map(CommentDTO::new)
				.collect(Collectors.toList());
		return new ResponseEntity<List<CommentDTO>>(commentDTOs, HttpStatus.OK);
	}
	
}
