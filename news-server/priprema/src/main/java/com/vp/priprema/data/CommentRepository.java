package com.vp.priprema.data;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vp.priprema.model.user.Comment;

public interface CommentRepository extends JpaRepository<Comment, Long> {

}
