package com.vp.priprema.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vp.priprema.data.CommentRepository;
import com.vp.priprema.model.user.Comment;

@Component
public class CommentService {
	
	@Autowired
	CommentRepository commentRepository;

	public List<Comment> findAll() {
		return commentRepository.findAll();
	}

	public <S extends Comment> S save(S entity) {
		return commentRepository.save(entity);
	}

	public Comment findById(Long id) {
		return commentRepository.findById(id).get();
	}
	
	
}
