package com.vp.priprema.web.dto;

import java.util.List;
import java.util.stream.Collectors;

import com.vp.priprema.model.user.Comment;
import com.vp.priprema.model.user.News;

public class CommentDTO {

	private Long id;
	
	private String username;

	private String text;

	private News news;
	
	List<CommentDTO> comments;
	
	public CommentDTO(Comment comment) {
		this.id = comment.getId();
		this.username = comment.getUsername();
		this.text = comment.getText();
		this.news = comment.getNews();
		this.comments = comment.getComments().stream().map(CommentDTO::new).collect(Collectors.toList());
	}

	public CommentDTO(String username, String text, News news) {
		super();
		this.username = username;
		this.text = text;
		this.news = news;
	}

	public CommentDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<CommentDTO> getComments() {
		return comments;
	}

	public void setCommentDTOs(List<CommentDTO> commentDTOs) {
		this.comments = commentDTOs;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}
	
}
