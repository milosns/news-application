package com.vp.priprema.model.user;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class News {
	
	@Id
	@GeneratedValue
	private Long id;
	private String name; 
	private String description;
	
	@Column(length = 500)
	private String content; 
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Category category;
	
	@JsonIgnore
	@OneToMany(mappedBy = "news", fetch = FetchType.LAZY)
	private Set<Comment> comments = new HashSet<Comment>();
	
	public News() {
		super();
	}

	public News(Long id, String name, String description, String content, Category category) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.content = content;
		this.category = category;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "News [id=" + id + ", name=" + name + ", description=" + description + ", content=" + content
				+ ", category=" + category + "]";
	} 
	
}
