use dbnews;

-- insert users
-- password is 12345 (bcrypt encoded) 
insert into security_user (id, username, password, first_name, last_name) values 
	(1, 'admin', '$2a$04$4pqDFh9SxLAg/uSH59JCB.LwIS6QoAjM9qcE7H9e2drFuWhvTnDFi', 'Admin', 'Admin');
-- password is abcdef (bcrypt encoded)
insert into security_user (id, username, password, first_name, last_name) values 
	(2, 'petar', '$2a$04$Yr3QD6lbcemnrRNLbUMLBez2oEK15pdacIgfkvymQ9oMhqsEE56EK', 'Petar', 'Petrovic');

-- insert authorities
insert into security_authority (id, name) values (1, 'ROLE_ADMIN'); -- super user
insert into security_authority (id, name) values (2, 'ROLE_USER'); -- normal user

-- insert mappings between users and authorities
insert into security_user_authority (id, user_id, authority_id) values (1, 1, 1); -- admin has ROLE_ADMIN
insert into security_user_authority (id, user_id, authority_id) values (2, 1, 2); -- admin has ROLE_USER too
insert into security_user_authority (id, user_id, authority_id) values (3, 2, 2); -- petar has ROLE_USER

INSERT INTO `dbnews`.`category` (`id`, `name`) VALUES ('1', 'politika');
INSERT INTO `dbnews`.`category` (`id`, `name`) VALUES ('2', 'filmovi');
INSERT INTO `dbnews`.`category` (`id`, `name`) VALUES ('3', 'zabava');
INSERT INTO `dbnews`.`category` (`id`, `name`) VALUES ('4', 'crna hronika');

INSERT INTO `dbnews`.`news` (`id`, `content`, `description`, `name`, `category_id`) VALUES ('1', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient monte', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 'News 1', '1');
INSERT INTO `dbnews`.`news` (`id`, `content`, `description`, `name`, `category_id`) VALUES ('2', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient monte', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 'News 1', '2');
INSERT INTO `dbnews`.`news` (`id`, `content`, `description`, `name`, `category_id`) VALUES ('3', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient monte', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 'News 1', '3');
INSERT INTO `dbnews`.`news` (`id`, `content`, `description`, `name`, `category_id`) VALUES ('4', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient monte', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 'News 1', '4');
INSERT INTO `dbnews`.`news` (`id`, `content`, `description`, `name`, `category_id`) VALUES ('5', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient monte', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 'News 1', '1');
INSERT INTO `dbnews`.`news` (`id`, `content`, `description`, `name`, `category_id`) VALUES ('6', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient monte', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 'News 1', '2');
INSERT INTO `dbnews`.`news` (`id`, `content`, `description`, `name`, `category_id`) VALUES ('7', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient monte', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 'News 1', '3');
INSERT INTO `dbnews`.`news` (`id`, `content`, `description`, `name`, `category_id`) VALUES ('8', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient monte', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 'News 1', '4');
INSERT INTO `dbnews`.`news` (`id`, `content`, `description`, `name`, `category_id`) VALUES ('9', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient monte', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 'News 1', '1');
INSERT INTO `dbnews`.`news` (`id`, `content`, `description`, `name`, `category_id`) VALUES ('10', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient monte', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 'News 1', '2');
INSERT INTO `dbnews`.`news` (`id`, `content`, `description`, `name`, `category_id`) VALUES ('11', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient monte', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 'News 1', '3');
INSERT INTO `dbnews`.`news` (`id`, `content`, `description`, `name`, `category_id`) VALUES ('12', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient monte', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 'News 1', '4');
