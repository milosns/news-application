import { NewsService } from './../../services/news.service';
import { Component, OnInit } from '@angular/core';
import { News } from 'src/app/model/news';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  private news: News[] = [];
  page = 0;

  constructor(private newsService: NewsService) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.newsService.getAll(this.page).subscribe(data => { this.news = data; });
  }

  nextPage() {
    this.page += 1;
    this.loadData();
  }

  prevPage() {
    if (this.page >= 0) {
      this.page -= 1;
      this.loadData();
    }
  }
}
