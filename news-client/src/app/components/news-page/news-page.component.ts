import { NewsService } from './../../services/news.service';
import { Component, OnInit } from '@angular/core';
import { News } from 'src/app/model/news';
import {Comment} from 'src/app/model/comment';
import { ActivatedRoute } from '@angular/router';
import { CommentService } from 'src/app/services/comment.service';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-news-page',
  templateUrl: './news-page.component.html',
  styleUrls: ['./news-page.component.css'],
})
export class NewsPageComponent implements OnInit {

  news: News;
  id: number;
  comments: Comment[];
  newComment : Comment;
  com: Comment;

  constructor(private newsService: NewsService,
    private commentService: CommentService,
    private route: ActivatedRoute) {
    this.getNews();
  }

  ngOnInit() {
    this.resetComment();
    this.resetCommentToComment();
  }

  getNews() {
    this.route.params.subscribe(param => {
      this.id = +param.id;
      this.newsService.getNews(this.id)
        .subscribe((res: News) => {
          this.news = res;
        })
      this.commentService.getAllComments(this.id)
        .subscribe((res: any) => {
          this.comments = res;
        })
    })
  }

  postComment() {
      this.commentService.postComment(this.id, this.newComment)
        .subscribe((res: any) => {
          this.getNews();
          this.resetComment();
        });
  }

  postCommentForComment(commentId: number) {
    this.commentService.postCommentForComment(this.id, commentId, this.com )
      .subscribe((res:any) => {
        this.getNews();
        this.resetCommentToComment();
      })
  }

  resetComment() {
    this.newComment = new Comment({
      username : '',
      text: ''
    })
  }

  resetCommentToComment() {
    this.com = new Comment({
      username: '',
      text: ''
    })
  }
}
