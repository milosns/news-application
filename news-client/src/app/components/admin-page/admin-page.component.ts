import { NewsService } from './../../services/news.service';
import { Component, OnInit } from '@angular/core';
import { News } from 'src/app/model/news';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {

  private news: News[] = [];
  page = 0;

  constructor(private newsService: NewsService) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.newsService.getAll(this.page).subscribe(data => { this.news = data; });
  }

  addNews(news:News) {
    this.newsService.saveNews(news).subscribe(data => this.loadData());
  }

  editNews(news: News) {
    this.newsService.editNews(news).subscribe(data => this.loadData());
  }

  deleteNews(id:number) {
    this.newsService.deleteNews(id).subscribe(data => this.loadData());
  }



  nextPage() {
    this.page += 1;
    this.loadData();
  }

  prevPage() {
    if (this.page >= 0) {
      this.page -= 1;
      this.loadData();
    }
  }



}
